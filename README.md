# brewers-log

A new GUI for the Brewing Control System API <br/>
ref: http://docs.embeddedcc.com/ <br/>
ref: https://www.embeddedcc.com/api-docs/ <br/>
Intended to run on a Raspberry Pi as a controller for a 3-kettle home brewing system <br/>
We may also read or reference this project: https://github.com/cscade/BCS.client

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
